package com.cognizant.carnival.domain;

import javax.persistence.*;


/**
 * The persistent class for the t_quiz database table.
 * 
 */
@Entity
@Table(name="t_quiz")
@NamedQuery(name="TQuiz.findAll", query="SELECT t FROM TQuiz t")
public class TQuiz  {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int questionid;

	private String answer1;

	private String answer2;

	private String answer3;

	private String answer4;

	private String correctanswer;

	@Lob
	private String description;

	@Lob
	private String question;

	@Lob
	@Column(name="success_text")
	private String successText;

	@Lob
	@Column(name="url_picture")
	private String urlPicture;

	public TQuiz() {
	}

	public int getQuestionid() {
		return this.questionid;
	}

	public void setQuestionid(int questionid) {
		this.questionid = questionid;
	}

	public String getAnswer1() {
		return this.answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getAnswer2() {
		return this.answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getAnswer3() {
		return this.answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	public String getAnswer4() {
		return this.answer4;
	}

	public void setAnswer4(String answer4) {
		this.answer4 = answer4;
	}

	public String getCorrectanswer() {
		return this.correctanswer;
	}

	public void setCorrectanswer(String correctanswer) {
		this.correctanswer = correctanswer;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getSuccessText() {
		return this.successText;
	}

	public void setSuccessText(String successText) {
		this.successText = successText;
	}

	public String getUrlPicture() {
		return this.urlPicture;
	}

	public void setUrlPicture(String urlPicture) {
		this.urlPicture = urlPicture;
	}

}