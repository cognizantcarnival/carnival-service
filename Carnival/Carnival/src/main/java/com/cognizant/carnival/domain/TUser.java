package com.cognizant.carnival.domain;

import javax.persistence.*;


/**
 * The persistent class for the t_user database table.
 * 
 */
@Entity
@Table(name="t_user")
@NamedQuery(name="TUser.findAll", query="SELECT t FROM TUser t")
public class TUser  {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userid;

	private String email;

	@Column(name="mit_code")
	private String mitCode;

	private int mobile;

	private String name;

	@Lob
	@Column(name="wt_will_je_doen")
	private String wtWillJeDoen;

	public TUser() {
	}

	public int getUserid() {
		return this.userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMitCode() {
		return this.mitCode;
	}

	public void setMitCode(String mitCode) {
		this.mitCode = mitCode;
	}

	public int getMobile() {
		return this.mobile;
	}

	public void setMobile(int mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWtWillJeDoen() {
		return this.wtWillJeDoen;
	}

	public void setWtWillJeDoen(String wtWillJeDoen) {
		this.wtWillJeDoen = wtWillJeDoen;
	}

}