package com.cognizant.carnival.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.cognizant.carnival.domain.TProgram;

public interface TProgramRepository extends CrudRepository<TProgram, Integer> {
	@Query("from TProgram where programdate >= NOW() ORDER BY programdate ASC")
	List<TProgram> upcoming();
	
	@Query("from TProgram where programdate < NOW()")
	List<TProgram> expired();
}
