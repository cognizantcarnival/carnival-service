package com.cognizant.carnival.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.cognizant.carnival.domain.TNews;

public interface TNewsRepository extends CrudRepository<TNews, Integer> {
	//A sample date format '2017-02-03-19:10:00' [february 3rd 2017]
	 @Query("from TNews where date >= CURDATE() ORDER BY date ASC") 
	 List<TNews> current(); 

	 @Query("from TNews where date < CURDATE()") 
	 List<TNews> expired(); 

}
