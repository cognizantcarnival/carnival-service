package com.cognizant.carnival.domain;

import javax.persistence.*;


/**
 * The persistent class for the t_proklemasie database table.
 * 
 */
@Entity
@Table(name="t_proklemasie")
@NamedQuery(name="TProklemasie.findAll", query="SELECT t FROM TProklemasie t")
public class TProklemasie  {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Lob
	private String proklemasie;

	public TProklemasie() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProklemasie() {
		return this.proklemasie;
	}

	public void setProklemasie(String proklemasie) {
		this.proklemasie = proklemasie;
	}

}