package com.cognizant.carnival.repositories;

import org.springframework.data.repository.CrudRepository;

import com.cognizant.carnival.domain.TProklemasie;

public interface TProklemasieRepository extends CrudRepository<TProklemasie, Integer> {

}
