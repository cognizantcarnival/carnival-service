package com.cognizant.carnival.repositories;

import org.springframework.data.repository.CrudRepository;

import com.cognizant.carnival.domain.TQuiz;

public interface TQuizRepository extends CrudRepository<TQuiz, Integer> {

}
