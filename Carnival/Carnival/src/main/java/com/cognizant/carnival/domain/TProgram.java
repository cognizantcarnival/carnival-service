package com.cognizant.carnival.domain;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_program database table.
 * 
 */
@Entity
@Table(name="t_program")
@NamedQuery(name="TProgram.findAll", query="SELECT t FROM TProgram t")
public class TProgram  {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int programid;

	private String address;

	@Lob
	private String description;

	@Column(name="program_name")
	private String programName;

//	@Temporal(TemporalType.TIMESTAMP)
	private Date programdate;

	@Lob
	@Column(name="url_img_banner")
	private String urlImgBanner;

	@Lob
	@Column(name="url_img_main")
	private String urlImgMain;

	public TProgram() {
	}

	public int getProgramid() {
		return this.programid;
	}

	public void setProgramid(int programid) {
		this.programid = programid;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProgramName() {
		return this.programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public Date getProgramdate() {
		return this.programdate;
	}

	public void setProgramdate(Date programdate) {
		this.programdate = programdate;
	}

	public String getUrlImgBanner() {
		return this.urlImgBanner;
	}

	public void setUrlImgBanner(String urlImgBanner) {
		this.urlImgBanner = urlImgBanner;
	}

	public String getUrlImgMain() {
		return this.urlImgMain;
	}

	public void setUrlImgMain(String urlImgMain) {
		this.urlImgMain = urlImgMain;
	}

}