package com.cognizant.carnival.domain;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the t_news database table.
 * 
 */
@Entity
@Table(name="t_news")
@NamedQuery(name="TNews.findAll", query="SELECT t FROM TNews t")
public class TNews  {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int newsid;

	@Temporal(TemporalType.DATE)
	private Date date;

	@Lob
	@Column(name="detail_news")
	private String detailNews;

	private String heading;

	@Lob
	@Column(name="url_bannerpicture")
	private String urlBannerpicture;

	@Lob
	@Column(name="url_mainpicture")
	private String urlMainpicture;

	public TNews() {
	}

	public int getNewsid() {
		return this.newsid;
	}

	public void setNewsid(int newsid) {
		this.newsid = newsid;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDetailNews() {
		return this.detailNews;
	}

	public void setDetailNews(String detailNews) {
		this.detailNews = detailNews;
	}

	public String getHeading() {
		return this.heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getUrlBannerpicture() {
		return this.urlBannerpicture;
	}

	public void setUrlBannerpicture(String urlBannerpicture) {
		this.urlBannerpicture = urlBannerpicture;
	}

	public String getUrlMainpicture() {
		return this.urlMainpicture;
	}

	public void setUrlMainpicture(String urlMainpicture) {
		this.urlMainpicture = urlMainpicture;
	}

}