package com.cognizant.carnival.repositories;

import org.springframework.data.repository.CrudRepository;

import com.cognizant.carnival.domain.TParticipant;

public interface TParticipantRepository extends CrudRepository<TParticipant, Integer> {

}
