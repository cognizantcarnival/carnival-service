package com.cognizant.carnival.repositories;

import org.springframework.data.repository.CrudRepository;

import com.cognizant.carnival.domain.TUser;

public interface TUserRepository extends CrudRepository<TUser, Integer> {

}
