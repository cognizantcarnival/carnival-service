package com.cognizant.carnival.domain;

import javax.persistence.*;


/**
 * The persistent class for the t_participant database table.
 * 
 */
@Entity
@Table(name="t_participant")
@NamedQuery(name="TParticipant.findAll", query="SELECT t FROM TParticipant t")
public class TParticipant  {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int participantid;

	private int age;

	private String declaration;

	@Lob
	private String description;

	private String hobbies;

	private String name;

	private String role;

	@Lob
	@Column(name="url_largeimage")
	private String urlLargeimage;

	@Lob
	@Column(name="url_smallimage")
	private String urlSmallimage;

	public TParticipant() {
	}

	public int getParticipantid() {
		return this.participantid;
	}

	public void setParticipantid(int participantid) {
		this.participantid = participantid;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getDeclaration() {
		return this.declaration;
	}

	public void setDeclaration(String declaration) {
		this.declaration = declaration;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHobbies() {
		return this.hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUrlLargeimage() {
		return this.urlLargeimage;
	}

	public void setUrlLargeimage(String urlLargeimage) {
		this.urlLargeimage = urlLargeimage;
	}

	public String getUrlSmallimage() {
		return this.urlSmallimage;
	}

	public void setUrlSmallimage(String urlSmallimage) {
		this.urlSmallimage = urlSmallimage;
	}

}