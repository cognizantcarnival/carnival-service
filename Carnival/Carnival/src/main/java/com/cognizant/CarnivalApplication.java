package com.cognizant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarnivalApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarnivalApplication.class, args);
	}
}
